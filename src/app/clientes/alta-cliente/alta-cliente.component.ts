import { ClientesService } from './../clientes.service';
import { Cliente, Grupo } from './../cliente.module';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alta-cliente',
  templateUrl: './alta-cliente.component.html',
  styleUrls: ['./alta-cliente.component.css']
})
export class AltaClienteComponent implements OnInit {

  cliente: Cliente;
  grupos: Grupo[];

  constructor(private clientesService: ClientesService) {

    this.cliente = {
      id: 0,
      nombre: '',
      cif: '',
      direccion: '',
      grupo: 0
    };
    this.grupos = [];

  }

  ngOnInit(): Promise<void> {

    this.cliente = this.clientesService.nuevoCliente();
    this.grupos = this.clientesService.getGrupos();

    return Promise.resolve();


  }

  nuevoCliente(): void {
    this.clientesService.agregarCliente(this.cliente);
    this.cliente = this.clientesService.nuevoCliente();
  }
}


